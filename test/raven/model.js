topo = {
  name: 'rotorhouse',
  nodes: [ td('controller'), deb('sim') ],
  links: [
    Link('controller', 1, 'sim', 1),
  ]
}

function deb(name) {
  return {
    name: name,
    image: 'debian-buster-td',
    cpu: { cores: 2 },
    memory: { capacity: GB(4) },
    mounts: [{ source: env.PWD+'/../..', point: '/tmp/cypress' }]
  }
}

function td(name, tdf) {
  x = deb(name)
  x.tdf = tdf
  return x
}
