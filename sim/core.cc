#include <iostream>
#include <cypress/sim/sim.hh>
#include <cypress/sim/core.hh>

using std::endl;
using std::cout;


Object::Object(size_t n, string label) 
  : r{n}, name{label} 
{
  Sim::get().objects.push_back(this);
}

bool Object::conditionsCheck()
{
  resid();
  bool ok{true};
  for(ulong i=0; i<r.n; ++i)
  {
    realtype ri = r(i);
    if(std::abs(ri) > 1e-9)
    {
      Sim::get().lg << name << ": r(" << i << ") = " << ri << endl;
      ok = false;
    }
  }
  return ok;
}

void Object::dumpState()
{
  Sim &sim = Sim::get();
  for(ulong i=0; i<r.n; ++i)
  {
    cout << "r("<<i<<") = " << r(i) << endl;
  }
  for(ulong i=r.idx; i<r.idx+r.n; ++i)
  {
    cout << sim.labels[i] << " = " << sim.y[i] << ", " << sim.dy[i] << endl;
  }
}

// Var ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Var::operator realtype& () { return Sim::get().y[idx]; }

// set this variables value
void Var::operator()(realtype &r) { Sim::get().y[idx] = r; }

// get this variables derivative value
realtype& Var::d() { return Sim::get().dy[idx]; }

// set this variables derivative value
void Var::d(realtype r) { Sim::get().dy[idx] = r; }

Var::Var(string label) 
{
  // grab an index for this variable from the simulator
  idx = Sim::get().nextVarIndex();

  // initialize this variables label with the simulator
  Sim::get().labels.push_back(label);
}

realtype & d(Var &x) { return x.d(); }

// Resid ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Resid::Resid(size_t n)
  : n{n}
{
  idx = Sim::get().nextResidIndex(n);
}

// assign a value to the residual vector and increment the current index
void Resid::operator=(realtype value)
{
  //std::cout << "r("<<idx+current<<") =" << value << std::endl;
  Sim::get().r[idx+current] = value;
  current = (current + 1) % n;
}

realtype Resid::operator()(size_t i)
{
  return Sim::get().r[idx+i];
}

// Binding ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Binding::Binding(Var a, Var b)
  : a{a}, b{b}
{
  idx = Sim::get().nextResidIndex(2);
  Sim::get().bindings.push_back(this);
}

inline realtype & Binding::r(size_t i) { return Sim::get().r[idx+i]; }

void Binding::resid()
{
  r(0) = a - b;
  r(1) = d(a) - d(b);
}
