#include <string.h>
#include <unistd.h>
#include <cstdlib>
#include <iostream>
#include <ida/ida.h>
#include <ida/ida_direct.h>
#include <cypress/sim/singledirect.hh>
#include <cypress/sim/sim.hh>

using std::ostream;
using std::cout;
using std::endl;
using std::ceil;
using std::endl;
using std::ofstream;
using std::ios_base;
using std::to_string;
using std::chrono::high_resolution_clock;
using std::chrono::duration_cast;
using std::chrono::microseconds;

int SingleDirect::run(realtype begin, realtype end, realtype step)
{
  lg << log("running simulation") << endl;
  lg << log("N=" + to_string(sim->yx)) << endl;

  initState();
  sim->initObjects();
  initIda(begin);

  bool ok = sim->conditionsCheck();
  if(!ok)
  {
    lg << log("conditions check failed, "
              "see the simulator log file for details") << endl;
    exit(1);
  }

  realtype tret{0};
 
  size_t result_size{static_cast<size_t>(ceil((end-begin)/step))};

  realtype *results = new realtype[result_size*(sim->yx*2+1)];
  size_t period = ceil(step / 1.0e-6);

  lg << log("begin crunch step=") << step << endl;

  size_t i{0};
  for(realtype t=begin+step; t<end; t += step, ++i)
  {
    sim->t = t;
    auto wt0 = high_resolution_clock::now();
    int retval = IDASolve(mem, t, &tret, yv, dyv, IDA_NORMAL);
    auto wt01 = high_resolution_clock::now();
    if(retval != IDA_SUCCESS)
    {
      lg << log("["+to_string(t)+"] IDASolve failed: " + to_string(retval)) 
         << endl;
      
      lg << log("running conditions check to help isolate") << endl;
      sim->conditionsCheck();
      results[i*(sim->yx*2+1)] = tret;
      memcpy(&results[i*(sim->yx*2+1)+1], sim->y, sizeof(realtype)*sim->yx);
      memcpy(&results[i*(sim->yx*2+1)+1+sim->yx], sim->dy, sizeof(realtype)*sim->yx);
      writeResults(results, i+1);
      for(Object *o : Sim::get().objects) o->dumpState();
      delete[] results;

      exit(1);
    }
    if(DEBUG) lg << log("ida finished") << endl;
    sim->t = t;
    sim->step();
    
    sim->slk.unlock();
    results[i*(sim->yx*2+1)] = t;
    memcpy(&results[i*(sim->yx*2+1)+1], sim->y, sizeof(realtype)*sim->yx);
    memcpy(&results[i*(sim->yx*2+1)+1+sim->yx], sim->dy, sizeof(realtype)*sim->yx);
    auto wt1 = high_resolution_clock::now();
    size_t selapsed = duration_cast<microseconds>(wt01 - wt0).count();
    size_t elapsed = duration_cast<microseconds>(wt1 - wt0).count();

    lg << log("solver time: ") << selapsed << ", " << elapsed << endl;

    int64_t wait = 1e6*step*tdf - elapsed;
    //std::cout << "wait: " << wait << std::endl;
    
    if(wait > 0) {
      usleep(wait);
    }
    sim->slk.lock();
  }

  cout << "simulation finished writing results" << endl;
  writeResults(results, i);

  delete[] results;

  return 0;
}

int F_Single(realtype /*t*/, N_Vector y, N_Vector dy, N_Vector r, void* /*udata*/)
{
  Sim::get().y = N_VGetArrayPointer(y);
  Sim::get().dy = N_VGetArrayPointer(dy);
  Sim::get().r = N_VGetArrayPointer(r);

  Sim::get().step();

  return 0;
}

void SingleDirect::initState()
{
  yv = N_VNew_Serial(sim->yx); 
  dyv = N_VNew_Serial(sim->yx);
  rv = N_VNew_Serial(sim->rx);

  sim->y = N_VGetArrayPointer(yv); 
  sim->dy = N_VGetArrayPointer(dyv);
  sim->r = N_VGetArrayPointer(rv);

  for(unsigned long i=0; i<sim->yx; ++i)
  {
    sim->y[i] = sim->dy[i] = 0;
  }

  lg << log("init state ok") << endl;
}

void SingleDirect::initIda(realtype begin)
{
  mem = IDACreate();
  if(mem == nullptr)
  {
    lg << log("IDACreate() failed") << endl;
    exit(1);
  }

  int retval = IDAInit(mem, F_Single, begin, yv, dyv);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDAInit() failed") << endl;
    exit(1);
  }

  retval = IDASetUserData(mem, this);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASetUserData() failed") << endl;
    exit(1);
  }

  retval = IDASStolerances(mem, rtl, atl);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASStolerances() failed") << endl;
    exit(1);
  }

  if(sim->rx != sim->yx)
  {
    lg << log("Number of equations must equal number of variables") << endl;
    lg << log("NEQ="+to_string(sim->rx)+" NV="+to_string(sim->yx)) << endl;
    exit(1);
  }

  lg << log("building matrix ") << sim->rx << "x" << sim->rx << endl;
  A = SUNDenseMatrix(sim->rx, sim->rx);
  if(A == nullptr)
  {
    lg << log("SUNDenseMatrix() failed") << endl;
    exit(1);
  }

  LS = SUNLinSol_Dense(yv, A);
  if(LS == nullptr)
  {
    lg << log("SUNLinSol_Dense() failed") << endl;
    exit(1);
  }

  retval = IDASetLinearSolver(mem, LS, A);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASetLinearSolver() failed") << endl;
    exit(1);
  }

  NLS = SUNNonlinSol_Newton(yv);
  if(NLS == nullptr)
  {
    lg << log("SUNLinSol_Newton() failed") << endl;
    exit(1);
  }

  retval = IDASetNonlinearSolver(mem, NLS);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASetNonlinearSolver() failed") << endl;
    exit(1);
  }

  retval = IDASetMaxNumSteps(mem, 500000);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASetMaxNumSteps() failed") << endl;
    exit(1);
  }

  /*
   * this may not be necessary, setting to 1e-6 makes simulations go very slow
   * as this is essentially decaying to an R-K type integrator.
  retval = IDASetMaxStep(mem, 1e-6);
  if(retval != IDA_SUCCESS)
  {
    lg << log("IDASetMaxStep() failed") << endl;
    exit(1);
  }
  */

  lg << log("IDAInit ok") << endl;
}

void SingleDirect::writeResults(realtype *results, size_t n)
{
  ofstream r_out{"results.csv", ios_base::out};
  lg << log("Simulation completed, saving results") << endl;
  r_out << "t," << sim->header() << endl;
  //r_out << sim->datastring() << endl;
  for(unsigned long j=0; j<n; ++j)
  {
    for(unsigned long k=0; k<(sim->yx*2+1)-1; ++k)
    {
      r_out << results[j*(sim->yx*2+1) + k] << ",";
    }
    r_out << results[j*(sim->yx*2+1)-1];
    r_out << endl;
  }
}


void SingleDirect::dumpState(ostream &out)
{
  out << "y" << endl;
  for(unsigned long k=0; k<sim->yx; ++k)
  {
    out << sim->y[k] << ",";
  }
  out << endl;
  
  out << "dy" << endl;
  for(unsigned long k=0; k<sim->yx; ++k)
  {
    out << sim->dy[k] << ",";
  }
  out << endl;
}
