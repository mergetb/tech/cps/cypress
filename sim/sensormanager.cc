#include <sys/socket.h>
#include <iostream>
#include <cypress/sim/core.hh>
#include <cypress/sim/sensormanager.hh>
#include <cypress/control/controller.hh>
#include <cypress/control/protocol.hh>

using std::cerr;
using std::endl;
  
SensorManager::SensorManager(Sim *sim)
  : sim{sim} 
{
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sockfd < 0)
  {
    cerr << "[" << t << "]" << "[SensorManager] socket() failure: " << sockfd << endl;
    exit(1);
  }
}

void SensorManager::add(Sensor &s)
{
  s.nxt = t + (1.0/s.rate);
  Q.push(&s);
}


bool SensorCmp::operator()(const Sensor *a, const Sensor *b)
{
  return a->nxt > b->nxt;
}

void SensorManager::step(double t)
{
  if(Q.empty()) return;

  this->t = t; 

  //XXX debugging code
  //cout << "[" << sim->t << "]" << "[sm] t=" << t << endl;
  //if there are no sensors the line below results in sadness
  //cout << "[" << sim->t << "]" << "[sm] q=" << Q.top()->nxt << endl;

 
  while(Q.top()->nxt <= (t-thresh))
  {
    Sensor *s = Q.top();
    tx(*s);
    s->nxt = t + (1.0/s->rate);
    Q.pop();
    Q.push(s);
  }
}

void SensorManager::tx(Sensor &s)
{
  realtype v = s.v;//sim->y[s.idx];
  unsigned long sec = std::floor(t);
  unsigned long usec = std::floor((t - sec)*1e6);

  CPacket cpk{s.id_tag, sec, usec, v};

  constexpr size_t sz{sizeof(CPacket)};
  char buf[sz];
  cpk.toBytes(buf);
  auto *addr = reinterpret_cast<sockaddr*>(&s.out_addr);
  sendto(sockfd, buf, sz, 0, addr, sizeof(s.out_addr));

  char sadr[128];
  inet_ntop(AF_INET, &(s.out_addr.sin_addr), sadr, 128);

  //XXX debugging code
  /*
  cout << "[" << sim->t  << "] "
    << sadr << ":" << ntohs(s.out_addr.sin_port) << "  " <<  v
    << endl;
  */
}
