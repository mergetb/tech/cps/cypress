#include <iostream>
#include <iomanip>
#include <cypress/elements/mechanical.hh>
#include <cypress/sim/sim.hh>

using namespace std;

struct RotorHouse
{
  string name;
  mech::Rotor r;
  Actuator a;
  Sensor s;

  RotorHouse(string name, unsigned long control_tag, string target)
    : name{name},
      r{name+".r", 25},
      a{name+".a", r.tau, {-10, 10}, {-0.5, 0.5}, control_tag},
      s{name+".s", r.w, 120, control_tag, target}
  { }
};

int main()
{
  RotorHouse a{"a", 1001, "localhost"};
  /*
  RotorHouse b{"b", 1002, "rcb"};
  RotorHouse c{"c", 1003, "rcc"};
  */

  Sim::get().hooks.push_back([&a](Sim &s){
      cout 
      << s.t << ": " 
      << fixed << setw(3) << setprecision(3) << a.r.w 
      << "                     \r" << flush;
  });

  return Sim::get().run(0, 47, 1e-3);
}

