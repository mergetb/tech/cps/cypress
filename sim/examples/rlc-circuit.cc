#include <cypress/elements/circuit.hh>

using namespace circuit;

int main() {

  ACVoltageSource s{"s", 10, 60};
  Resistor r{"r"};
  Inductor l{"l"};
  Capacitor c{"c"};
  Ground g{"g"};

  connect(s.p, r.n);
  connect(r.p, l.n);
  connect(l.p, c.n);
  connect(c.p, s.n);
  connect(g.z, s.n);

  Sim::get().run(0, 10, 1e-6);

}
