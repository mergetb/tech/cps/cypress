#include <string.h>
#include <cypress/control/protocol.hh>
#include <cypress/sim/actuationserver.hh>

using std::thread;
using std::endl;
using std::to_string;
using std::runtime_error;
using std::lock_guard;
using std::mutex;

ActuationServer::ActuationServer() 
{
  initComms();
  listen_thread = new thread([this](){ listen(); });
  listen_thread->detach();
}

void ActuationServer::enlist(Actuator &a)
{
 actuators[a.id_tag] = &a;
}

void ActuationServer::initComms()
{
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  if(sockfd < 0)
  {
    lg << log("socket() failed") << endl;
    exit(1);
  }
  bzero(&addr, sizeof(addr)); 
  addr.sin_family = AF_INET;
  addr.sin_addr.s_addr = htonl(INADDR_ANY);
  addr.sin_port = htons(port);

  auto *saddr = reinterpret_cast<const struct sockaddr*>(&addr);
  int err = bind(sockfd, saddr, sizeof(addr));
  if(err < 0)
  {
    lg << log("bind() failed "+to_string(err)) << endl;
    exit(1);
  }

  lg << log("bound to port "+to_string(port)) << endl;
}

void ActuationServer::listen()
{
  socklen_t len;
  constexpr size_t sz{sizeof(CPacket)};
  char msg[sz];
  sockaddr_in client_addr;
  auto *caddr = reinterpret_cast<sockaddr*>(&client_addr);

  lg << log("listening") << endl;

  for(;;)
  {
    int err = recvfrom(sockfd, msg, sz, 0, caddr, &len);
    if(err < 0)
    {
      lg << log("recvfrom() failed: "+to_string(err)) << endl;
      exit(1);
    }

    CPacket pkt;
    try { pkt = CPacket::fromBytes(msg); }
    catch(runtime_error &ex)
    {
      lg << log("packet read error: " + string(ex.what())) << endl;
      continue;
    }

    lock_guard<mutex> lk(rx_mtx);
    try { actuators.at(pkt.id_tag)->actuate(pkt.value); }
    catch(std::out_of_range)
    {
      lg << log("unknown id_tag "+to_string(pkt.id_tag)) << endl;
      continue;
    }
  }
}
