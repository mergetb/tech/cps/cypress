#include <cypress/sim/sim.hh>
#include <iostream>

using std::endl;

void Sim::initObjects()
{
  for(Object *o : objects) o->init();
}

void Sim::step()
{
  sensorManager.step(t);
  for(Object *o : objects)   o->resid();
  for(Binding *b : bindings) b->resid();
  for(Hook h: hooks) h(*this);
}

bool Sim::conditionsCheck()
{
  lg << log("Checking conditions") << endl;
  bool ok{true};
  for(Object *o : objects)
  {
    ok = ok && o->conditionsCheck();
  }
  if(ok) lg << log("Conditions look good!") << endl;
  else   lg << log("Conditions check failed") << endl;

  return ok;
}

string Sim::header()
{
  string s;
  for(string &l: labels)
  {
    s += l + ",";
  }
  for(size_t i=0; i<labels.size()-1; ++i)
  {
    s += labels[i] + "',";
  }
  s += labels[labels.size()-1] + "'";
  return s;
}
