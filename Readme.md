# Cypress

Cypress is a physical simulation engine that is designed to interact with cyber
emulation environments - enabling the creation of cyber-physical system
experiments.

With Cypress you can

- Model physical systems in C++
  + Physical objects are sets of equations encapsulated in a C++ struct.
  + Objects have **nodes** that allow variables across objects to be **linked**
    into a system of objects.
  + Sensors read variables at each time step in a simulation and push value
    streams out through multicast at user defined rates.
  + Actuators allow external control of simulated variables from a network.

- Run simulations at a specified time-dilation factor.
  + Tell the simulator how fast simulation time should evolve realtive to wall
    clock time.
  + Allows for integration with time-dilated cyber systems.

## Building

You'll need the following installed

- cmake
- ninja (the build tool)
- clang++
- libyaml-cpp

### Build and install Sundials

We keep sundials as a versioned submodule. First ensure you have it.

```shell
git submodule init
git submodule update
```

Then build and install sundials

```shell
cd 3p/sundials
mkdir build
cmake .. -G Ninja
ninja
sudo ninja install
```
### Build Cypress

Now from the top level source directory, build Cypress.

```shell
mkdir build
cd build
cmake .. -G Ninja
ninja
```
