#pragma once

#include <cmath>
#include <cypress/sim/sim.hh>

using std::pow;

namespace mech {

struct Rotor : public Object {
  Var w, tau, theta;
  realtype H;

  Rotor(string label, realtype H)
    : Object{2, label}, 
      w{label+".w"},
      tau{label+".tau"},
      theta{label+".theta"},
      H{H}
  {}

  void resid() override
  {
    r = d(w) - (tau - H*pow(w, 2.0));
    r = d(theta) - w;
  }
  
};

}
