#pragma once

#include <cypress/sim/sim.hh>

namespace circuit {

struct Terminal {

  Var i, e;

  Terminal(string label) :
    i{label+".i"},
    e{label+".e"}
  {}

};

void connect(Terminal &a, Terminal &b)
{
  bind(a.i, b.i);
  bind(a.e, b.e);
}

struct Branch : Object {

  Terminal p, n;
  Var v;

  Branch(size_t n, string label) 
    : Object{2+n, label},
      p{label+".p"},
      n{label+".n"},
      v{label+".v"}
  {}

  virtual void resid() override 
  {
    r = p.e - n.e - v;
    r = p.i - n.i;
  }

};

struct Resistor : Branch {

  realtype R;

  Resistor(string label, realtype R = 1e-6) 
    : Branch{1, label},
      R{R}
  {}

  void resid() override final 
  {
    Branch::resid();

    r = p.i*R - v;
  }

};

struct Ground : Object {

  Terminal z;

  Ground(string label = "g") 
    : Object{1, label}, 
      z{label+".z"}
  {}

  void resid() override final
  {
    r = z.e;
  }
};

struct Inductor : Branch {

  realtype L;

  Inductor(string label, realtype L = 1e-6) 
    : Branch{1, label},
      L{L}
  {}

  void resid() override final
  {
    Branch::resid();

    r = d(p.i)*L - v;
  }

};

struct Capacitor : Branch {

  realtype C;

  Capacitor(string label, realtype C = 1e-6) 
    : Branch{1, label},
      C{C}
  {}

  void resid() override final
  {
    Branch::resid();

    r = d(v)*C - p.i;
  }

};

struct ACVoltageSource : Branch {

  realtype A, // amplitude
           F; // frequency

  ACVoltageSource(string label, realtype A = 1, realtype F = 1) 
    : Branch{1, label},
      A{A},
      F{F}
  {}

  void resid() override final
  {
    Branch::resid();

    r = A*cos(F*2*M_PI*t()) - v;
  }

};

}
