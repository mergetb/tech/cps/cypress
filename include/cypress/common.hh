#pragma once

#include <string>
#include <vector>
#include <chrono>

// This library unapologetically uses standard library data structures in the
// base namespace. If you're clashing with these names, you're wrong.
using std::vector;
using std::string;

typedef double realtype;

struct Object;
struct Binding;
struct Var;
struct Sim;
struct SensorManager;
struct SingleDirect;

inline string log(string msg)
{
  std::time_t t = std::time(nullptr);
  char ts[128];
  std::strftime(ts, sizeof(ts), "%T", std::localtime(&t));
  
  return string("[") + string(ts) + "] " + msg;
}

inline string ts()
{
  std::time_t t = std::time(nullptr);
  char ts[128];
  std::strftime(ts, sizeof(ts), "%F %T", std::localtime(&t));
  
  return string("[") + string(ts) + string("] ");
}

