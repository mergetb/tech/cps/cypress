#pragma once

#include <queue>
#include <cypress/sim/core.hh>
#include <cypress/sim/sensor.hh>

struct SensorCmp
{
  bool operator()(const Sensor *a, const Sensor *b);
};

struct SensorManager {

  explicit SensorManager(Sim *sim);

  void step(realtype t);
  void add(Sensor&);
  void tx(Sensor&);

  private:
    Sim *sim;
    int sockfd;
    double t{0}, thresh{0.74e-6};
    std::priority_queue<Sensor*, std::vector<Sensor*>, SensorCmp> Q;

};
