#pragma once

#include <arpa/inet.h>
#include <cypress/sim/core.hh>

struct Sensor {

  Var v;

  size_t rate, id_tag;
  string name, target;
  realtype nxt;

  sockaddr_in out_addr;

  Sensor(string name, Var v, size_t rate, size_t id_tag, string target);

};
