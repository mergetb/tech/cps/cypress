#pragma once

#include <fstream>
#include <sundials/sundials_types.h>
#include <nvector/nvector_serial.h>
#include <sunmatrix/sunmatrix_dense.h>
#include <sunlinsol/sunlinsol_dense.h>
#include <sunnonlinsol/sunnonlinsol_newton.h>
#include <cypress/common.hh>

struct SingleDirect {

  SingleDirect(Sim *sim) : sim{sim} {}

  N_Vector yv, dyv, rv;

  // time dilation factor
  size_t tdf{1};

  int run(realtype begin, realtype end, realtype step);
  void dumpState(std::ostream &out);
  void writeResults(realtype *results, size_t n);

  private:
    Sim *sim{nullptr};
    std::ofstream lg{"solver.log", std::ios_base::out | std::ios_base::app};

    void *mem{nullptr};
    SUNMatrix A;
    SUNLinearSolver LS;
    SUNNonlinearSolver NLS;

    realtype rtl = RCONST(0.0), atl = RCONST(1.0e-8);

    void initState();
    void initIda(realtype begin);

};
