#pragma once

#include <functional>
#include <cypress/sim/core.hh>
#include <cypress/sim/singledirect.hh>
#include <cypress/sim/sensormanager.hh>
#include <cypress/sim/actuationserver.hh>

using Hook = std::function<void(Sim&)>;

#define DEBUG false

struct Sim {

  // DAE vector equation variables f(y, y', t) = r
  realtype *y{nullptr}, *dy{nullptr}, *r{nullptr}, t;

  // Indices into DAE variables, note that yx is always equal to dyx as y and dy
  // are just a variable and it's derivative so there is only a need to track
  // one.
  size_t yx{0}, rx{0};

  std::ofstream lg{"sim.log", std::ios_base::out | std::ios_base::app};

  std::mutex mtx;
  std::unique_lock<std::mutex> slk{mtx, std::try_to_lock};

  // a list of objects being simulated
  vector<Object*> objects;

  // a list of bindings between variables
  vector<Binding*> bindings;

  // A list of labels used for result formatting
  vector<string> labels;

  // A list of hooks that are executed at every simulation time step
  vector<Hook> hooks;

  // The sensor maanager manages the sensors that stream variable values to
  // network components.
  SensorManager sensorManager;
  ActuationServer actuationServer{};

  // The Simulation object follows the singleton pattern in that there is only
  // one per translation unit.
  static Sim & get()
  {
    static Sim instance;
    return instance;
  }

  // get a variable index
  size_t nextVarIndex() { return yx++; }

  // get @n residual indices
  size_t nextResidIndex(size_t n) { 
    size_t _rx = rx;
    rx += n;
    return _rx;
  }

  template<class Solver=SingleDirect>
  int run(realtype begin, realtype end, realtype step) 
  {
    Solver solver{this};
    return solver.run(begin, end, step);
  }

  void initObjects();
  void step();
  bool conditionsCheck();
  string header();

  private:
    Sim() : sensorManager{this} {}

};

inline realtype t() { return Sim::get().t; }
