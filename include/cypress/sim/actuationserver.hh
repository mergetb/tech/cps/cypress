#pragma once

#include <fstream>
#include <mutex>
#include <unordered_map>
#include <cypress/sim/actuator.hh>

struct ActuationServer
{
  explicit ActuationServer();

  void enlist(Actuator &a);

  private:
    std::ofstream 
      lg{"actuation_server.log", std::ios_base::out | std::ios_base::app};
    std::thread *listen_thread{nullptr};
    int sockfd;
    sockaddr_in addr;
    unsigned int port{4747};
    std::mutex rx_mtx;
    std::unordered_map<unsigned int, Actuator*> actuators;

    void initComms();
    void listen();
};
