#pragma once

#include <mutex>
#include <fstream>
#include <cmath>
#include <cypress/common.hh>

struct Var {

  // the index of this variable in a DAE system, e.g.
  // x  = sys.y[idx]
  // x' = sys.y[idx]
  size_t idx;

  // convert this variable into its value
  operator realtype& ();

  // set this variables value
  void operator ()(realtype &r);

  // get this variables derivative value
  realtype& d();

  // set this variables derivative value
  void d(realtype r);

  Var(string label = "");

};

realtype & d(Var &x);

struct Resid {
  
  // the base index of this residula in a DAE system e.g.
  // r = sys.r[idx]
  size_t idx;

  // the size of the residual in the vector sense e.g.
  // r[0] = sys.r[idx]
  // r[n] = sys.r[idx+n]
  size_t n;

  // the current element pointed to by this resid
  size_t current{0};

  Resid(size_t n);

  // assign a value to the residual vector and increment the current index
  void operator=(realtype value);

  realtype operator()(size_t i);


};

struct Binding {

  Var a, b;
  size_t idx;

  Binding(Var a, Var b);

  inline realtype & r(size_t i);
  void resid();

};

inline Binding bind(Var a, Var b)
{
  return Binding{a, b};
}

struct Object {

  // index into the DAE residual equation f(y, y', t) = r
  Resid r;
  string name;

  explicit Object(size_t n, string label);
  virtual ~Object() = default;
  virtual void init() { };

  virtual void resid() = 0;

  bool conditionsCheck();
  void dumpState();

};

